import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';
import MainPage from "./components/main/MainPage";
import Divepool from "./components/divepool/Divepool";
import Generator from "./components/generator/Generator";
import GripCounter from "./components/gripCounter/GripCounter";
import Quiz from "./components/quiz/Quiz";
import About from "./components/about/About";
import CategoryPage from "./components/divepool/CategoryPage";

const Stack = createStackNavigator();

export default function App() {
  return (
    
    <NavigationContainer>
      <Stack.Navigator initialRouteName="MainPage">
        <Stack.Screen name="MainPage" component={MainPage} />
        <Stack.Screen name="Divepool" component={Divepool} />
        <Stack.Screen name="Generator" component={Generator} />
        <Stack.Screen name="GripCounter" component={GripCounter} />
        <Stack.Screen name="Quiz" component={Quiz} />
        <Stack.Screen name="About" component={About} />
        <Stack.Screen name="CategoryPage" component={CategoryPage} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

// это очень похоже на css модули, которые есть у нас в реакте
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  testText: {
    color: "red",
    fontSize: 30,
  },
});
