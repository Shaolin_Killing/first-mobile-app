const BELLY_MULTIPLIER = 1

const BACK_MULTIPLIER = 2

const KNEES_MULTIPLIER = 4

const HEADUP_MULTIPLIER = 6

const HEADDOWN_MULTIPLIER = 8

const bellyFlying = [
  {
    id: 1,
    title: 'Neutral belly position',
    description: '',
    points: 5,
    multiplier: BELLY_MULTIPLIER,
  },
  {
    id: 2,
    title: 'Belly forward & backward',
    description: '',
    points: 5,
    multiplier: BELLY_MULTIPLIER,
  },
  {
    id: 3,
    title: 'Belly fly rotation turns',
    description: '',
    points: 5,
    multiplier: BELLY_MULTIPLIER,
  },
  {
    id: 4,
    title: 'Belly up & down',
    description: '',
    points: 5,
    multiplier: BELLY_MULTIPLIER,
  },
  {
    id: 5,
    title: 'Belly left & right slides',
    description: '',
    points: 5,
    multiplier: BELLY_MULTIPLIER,
  },
  {
    id: 6,
    title: 'Belly enter & exit',
    description: '',
    points: 5,
    multiplier: BELLY_MULTIPLIER,
  },
]

const backFlying = [
  {
    id: 7,
    title: 'Neutral back position',
    description: '',
    points: 5,
    multiplier: BACK_MULTIPLIER,
  },
  {
    id: 8,
    title: 'Back forward & backward',
    description: '',
    points: 5,
    multiplier: BACK_MULTIPLIER,
  },
  {
    id: 9,
    title: 'Back fly rotation turns',
    description: '',
    points: 5,
    multiplier: BACK_MULTIPLIER,
  },
  {
    id: 10,
    title: 'Back up & down',
    description: '',
    points: 5,
    multiplier: BACK_MULTIPLIER,
  },
  {
    id: 11,
    title: 'Back left & right slides',
    description: '',
    points: 5,
    multiplier: BACK_MULTIPLIER,
  },
  {
    id: 12,
    title: 'Back enter & exit',
    description: '',
    points: 5,
    multiplier: BACK_MULTIPLIER,
  },
  {
    id: 13,
    title: 'Half barrel roll',
    description: '',
    points: 5,
    multiplier: BACK_MULTIPLIER,
  },
  {
    id: 14,
    title: 'Belly back',
    description: '',
    points: 5,
    multiplier: BACK_MULTIPLIER,
  },
]

const headUpFlying = [
  {
    id: 15,
    title: 'High speed control',
    description: '',
    points: 5,
    multiplier: HEADUP_MULTIPLIER,
  },
  {
    id: 16,
    title: 'Transition to head up',
    description: '',
    points: 5,
    multiplier: HEADUP_MULTIPLIER,
  },
  {
    id: 17,
    title: 'Neutral sit position',
    description: '',
    points: 5,
    multiplier: HEADUP_MULTIPLIER,
  },
  {
    id: 18,
    title: 'Sit rotation turns',
    description: '',
    points: 5,
    multiplier: HEADUP_MULTIPLIER,
  },
  {
    id: 19,
    title: 'Sit forward & backward',
    description: '',
    points: 5,
    multiplier: HEADUP_MULTIPLIER,
  },
  {
    id: 20,
    title: 'Sit up & down',
    description: '',
    points: 5,
    multiplier: HEADUP_MULTIPLIER,
  },
  {
    id: 21,
    title: 'Sit left & right slides',
    description: '',
    points: 5,
    multiplier: HEADUP_MULTIPLIER,
  },
]

const headDownFlying = [
  {
    id: 22,
    title: 'Transit to head down',
    description: '',
    points: 5,
    multiplier: HEADDOWN_MULTIPLIER,
  },
  {
    id: 23,
    title: 'Head down rotation turns',
    description: '',
    points: 5,
    multiplier: HEADDOWN_MULTIPLIER,
  },
  {
    id: 24,
    title: 'Head down up & down',
    description: '',
    points: 5,
    multiplier: HEADDOWN_MULTIPLIER,
  },
  {
    id: 25,
    title: 'Head down forward & backward',
    description: '',
    points: 5,
    multiplier: HEADDOWN_MULTIPLIER,
  },
  {
    id: 26,
    title: 'Head down front transit',
    description: '',
    points: 5,
    multiplier: HEADDOWN_MULTIPLIER,
  },
]

const kneeFlying = [
  {
    id: 27,
    title: 'Transit to knees',
    description: '',
    points: 5,
    multiplier: KNEES_MULTIPLIER,
  },
  {
    id: 28,
    title: 'Knees neutral position',
    description: '',
    points: 5,
    multiplier: KNEES_MULTIPLIER,
  },
  {
    id: 29,
    title: 'Knees forward & backward',
    description: '',
    points: 5,
    multiplier: KNEES_MULTIPLIER,
  },
  {
    id: 30,
    title: 'Knees up & down',
    description: '',
    points: 5,
    multiplier: KNEES_MULTIPLIER,
  },
  {
    id: 31,
    title: 'Knees rotation turns',
    description: '',
    points: 5,
    multiplier: KNEES_MULTIPLIER,
  },
]

export const positions = [
  {
    title: 'belly',
    items: [...bellyFlying],
  },
  {
    title: 'back',
    items: [...backFlying],
  },
  {
    title: 'headup',
    items: [...headUpFlying],
  },
  {
    title: 'headdown',
    items: [...headDownFlying],
  },
  {
    title: 'knees',
    items: [...kneeFlying],
  }
]
