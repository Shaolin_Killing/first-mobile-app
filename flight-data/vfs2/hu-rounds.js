export default [
  {
    type: '',
    title: 'HU-A',
    description: 'Single grip',
    image: '/vfs2/HU/HU-A.png',
    quizImage: '/vfs2/quiz/HU/HU-A.jpg',

  },
  {
    type: '',
    title: 'HU-B',
    description: 'In-Facing double grip',
    image: '/vfs2/HU/HU-B.png',
    quizImage: '/vfs2/quiz/HU/HU-B.jpg',

  },
  {
    type: '',
    title: 'HU-C',
    description: 'Out-Facing double grip',
    image: '/vfs2/HU/HU-C.png',
    quizImage: '/vfs2/quiz/HU/HU-C.jpg',

  },
  {
    type: '',
    title: 'HU-D',
    description: 'Hand-to-foot',
    image: '/vfs2/HU/HU-D.png',
    quizImage: '/vfs2/quiz/HU/HU-D.jpg',

  },
  {
    type: '',
    title: 'HU-E',
    description: 'Hands-to-feet',
    image: '/vfs2/HU/HU-E.png',
    quizImage: '/vfs2/quiz/HU/HU-E.jpg',

  },
  {
    type: '',
    title: 'HU-F',
    description: 'Feet-to-knees',
    image: '/vfs2/HU/HU-F.png',
    quizImage: '/vfs2/quiz/HU/HU-F.jpg',

  },
  {
    type: '',
    title: 'HU-G',
    description: 'Totem',
    image: '/vfs2/HU/HU-G.png',
    quizImage: '/vfs2/quiz/HU/HU-G.jpg',

  },
  {
    type: '',
    title: 'HU-H',
    description: 'Foot to foot',
    image: '/vfs2/HU/HU-H.png',
    quizImage: '/vfs2/quiz/HU/HU-H.jpg',

  },
  {
    type: '',
    title: 'HU-J',
    description: 'Double spock',
    image: '/vfs2/HU/HU-J.png',
    quizImage: '/vfs2/quiz/HU/HU-J.jpg',
  },
  {
    type: 'block',
    title: 'HU-01',
    description: '360 block',
    image: '/vfs2/HU/HU-01.png',
    quizImage: '/vfs2/quiz/HU/HU-01.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'HU-02',
    description: 'Carve block',
    image: '/vfs2/HU/HU-02.png',
    quizImage: '/vfs2/quiz/HU/HU-02.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'HU-03',
    description: 'Flip block',
    image: '/vfs2/HU/HU-03.png',
    quizImage: '/vfs2/quiz/HU/HU-03.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'HU-04',
    description: 'Over/Under block',
    image: '/vfs2/HU/HU-04.png',
    quizImage: '/vfs2/quiz/HU/HU-04.jpg',
    cost: 2
  }
]
