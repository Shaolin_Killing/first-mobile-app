export default [
  {
    type: '',
    title: 'HD-A',
    description: 'Joker',
    image: '/vfs2/HD/HD-A.png',
    quizImage: '/vfs2/quiz/HD/HD-A.jpg',

  },
  {
    type: '',
    title: 'HD-B',
    description: 'In-Facing double grip',
    image: '/vfs2/HD/HD-B.png',
    quizImage: '/vfs2/quiz/HD/HD-B.jpg',

  },
  {
    type: '',
    title: 'HD-C',
    description: 'Mixed In-Facing double grip',
    image: '/vfs2/HD/HD-C.png',
    quizImage: '/vfs2/quiz/HD/HD-C.jpg',

  },
  {
    type: '',
    title: 'HD-D',
    description: 'Out-Facing double grip',
    image: '/vfs2/HD/HD-D.png',
    quizImage: '/vfs2/quiz/HD/HD-D.jpg',

  },
  {
    type: '',
    title: 'HD-E',
    description: 'Mind warp',
    image: '/vfs2/HD/HD-E.png',
    quizImage: '/vfs2/quiz/HD/HD-E.jpg',

  },
  {
    type: '',
    title: 'HD-F',
    description: 'Double spock',
    image: '/vfs2/HD/HD-F.png',
    quizImage: '/vfs2/quiz/HD/HD-F.jpg',

  },
  {
    type: '',
    title: 'HD-G',
    description: 'Sole to sole',
    image: '/vfs2/HD/HD-G.png',
    quizImage: '/vfs2/quiz/HD/HD-G.jpg',

  },
  {
    type: '',
    title: 'HD-H',
    description: 'Stair step',
    image: '/vfs2/HD/HD-H.png',
    quizImage: '/vfs2/quiz/HD/HD-H.jpg',

  },
  {
    type: '',
    title: 'HD-J',
    description: 'Vertical closed accordion',
    image: '/vfs2/HD/HD-J.png',
    quizImage: '/vfs2/quiz/HD/HD-J.jpg',
  },
  {
    type: '',
    title: 'HD-K',
    description: 'Sixty nine',
    image: '/vfs2/HD/HD-K.png',
    quizImage: '/vfs2/quiz/HD/HD-K.jpg',
  },
  {
    type: 'block',
    title: 'HD-01',
    description: 'Sixty nine block',
    image: '/vfs2/HD/HD-01.png',
    quizImage: '/vfs2/quiz/HD/HD-01.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'HD-02',
    description: '360 block',
    image: '/vfs2/HD/HD-02.png',
    quizImage: '/vfs2/quiz/HD/HD-02.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'HD-03',
    description: 'Carve block',
    image: '/vfs2/HD/HD-03.png',
    quizImage: '/vfs2/quiz/HD/HD-03.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'HD-04',
    description: 'HU/HD flip block',
    image: '/vfs2/HD/HD-04.png',
    quizImage: '/vfs2/quiz/HD/HD-04.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'HD-05',
    description: 'Half-eagle block',
    image: '/vfs2/HD/HD-05.png',
    quizImage: '/vfs2/quiz/HD/HD-05.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'HD-06',
    description: 'Half-cradle block',
    image: '/vfs2/HD/HD-06.png',
    quizImage: '/vfs2/quiz/HD/HD-06.jpg',
    cost: 2
  }
]
