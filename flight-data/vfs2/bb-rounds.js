export default [
  {
    type: '',
    title: 'BB-A',
    description: 'Belly star',
    image: '/vfs2/BB/BB-A.png',
    quizImage: '/vfs2/quiz/BB/BB-A.jpg',

  },
  {
    type: '',
    title: 'BB-B',
    description: 'Back star',
    image: '/vfs2/BB/BB-B.png',
    quizImage: '/vfs2/quiz/BB/BB-B.jpg',

  },
  {
    type: '',
    title: 'BB-C',
    description: 'Belly closed accordion',
    image: '/vfs2/BB/BB-C.png',
    quizImage: '/vfs2/quiz/BB/BB-C.jpg',

  },
  {
    type: '',
    title: 'BB-D',
    description: 'Mixed closed accordion',
    image: '/vfs2/BB/BB-D.png',
    quizImage: '/vfs2/quiz/BB/BB-D.jpg',

  },
  {
    type: '',
    title: 'BB-E',
    description: 'Back open accordion',
    image: '/vfs2/BB/BB-E.png',
    quizImage: '/vfs2/quiz/BB/BB-E.jpg',

  },
  {
    type: '',
    title: 'BB-F',
    description: 'Mixed open accordion',
    image: '/vfs2/BB/BB-F.png',
    quizImage: '/vfs2/quiz/BB/BB-F.jpg',

  },
  {
    type: '',
    title: 'BB-G',
    description: 'Back side body',
    image: '/vfs2/BB/BB-G.png',
    quizImage: '/vfs2/quiz/BB/BB-G.jpg',

  },
  {
    type: '',
    title: 'BB-H',
    description: 'Mixed side body',
    image: '/vfs2/BB/BB-H.png',
    quizImage: '/vfs2/quiz/BB/BB-H.jpg',

  },
  {
    type: '',
    title: 'BB-J',
    description: 'Back cat',
    image: '/vfs2/BB/BB-J.png',
    quizImage: '/vfs2/quiz/BB/BB-J.jpg',
  },
  {
    type: '',
    title: 'BB-K',
    description: 'Mixed cat',
    image: '/vfs2/BB/BB-K.png',
    quizImage: '/vfs2/quiz/BB/BB-K.jpg',
  },
  {
    type: '',
    title: 'BB-L',
    description: 'Back stair step',
    image: '/vfs2/BB/BB-L.png',
    quizImage: '/vfs2/quiz/BB/BB-L.jpg',
  },
  {
    type: 'block',
    title: 'BB-01',
    description: '360 block',
    image: '/vfs2/BB/BB-01.png',
    quizImage: '/vfs2/quiz/BB/BB-01.jpg',
    cost: 2
  },
  {
    type: 'block',
    title: 'BB-02',
    description: 'Over/Under block',
    image: '/vfs2/BB/BB-02.png',
    quizImage: '/vfs2/quiz/BB/BB-02.jpg',
    cost: 2
  }
]
