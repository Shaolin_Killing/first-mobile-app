export const disciplines = [
  {
    title: 'vfs2',
    type: 'vfs2',
    time: 35,
    linkToDivePool: '',
  },
  {
    title: 'vfs4',
    type: 'vfs4',
    time: 35,
    linkToDivePool: '',
    disabled: true
  },
  {
    title: 'fs2',
    type: 'fs2',
    time: 25,
    linkToDivePool: ''
  },
  {
    title: 'fs4',
    type: 'fs4',
    time: 35,
    linkToDivePool: '',
    disabled: true
  }
];
