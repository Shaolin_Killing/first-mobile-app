import React, { useState } from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, Modal, } from "react-native";
import {  TapGestureHandler, RotationGestureHandler, PanGestureHandler, State, GestureEvent} from "react-native-gesture-handler";
const names = ['BB-01', 'BB-02', 'BB-A', 'BB-B', 'BB-C', 'BB-D', 'BB-E', 'BB-F', 'BB-G', 'BB-H', 'BB-J', 'BB-K', 'BB-L'];

interface CardProps {
  imageName: string;
  image: any;
  style?: object;
  name: string;
}

const DivepoolCard: React.FC<CardProps> = ({ imageName, image, }) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(0);

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };


  return (
    
    <TouchableOpacity style={styled.oneCardWrapper} onPress={openModal}>
      <View style={styled.wrapperForCardText}>
        <View>
          <Text style={styled.bigText}>{imageName}</Text>
        </View>
        <View>
          <Text style={styled.smallText}>360 Блок</Text>
        </View>
      </View>

      <View style={styled.wrapperForImage}>
        <Image
          source={{ uri: image.localUri }}
          style={{ width: 70, height: 70 }}
        />
      </View>

      <Modal
        visible={modalVisible}
        transparent={true}
        onRequestClose={closeModal}
      >
        <View style={styled.modalContainer}>
          <Text>Это модальное окно</Text>

          <TapGestureHandler>
            <RotationGestureHandler>
              <View>
                <View style={styled.wrapperForImage}>
                  <Image
                    source={{ uri: image.localUri }}
                    style={{ width: 400, height: 400 }}
                  />
                </View>
                <View style={styled.wrapperForImage}>
                  <Text>{imageName}</Text>
                </View>
              </View>
            </RotationGestureHandler>
          </TapGestureHandler>

          <TouchableOpacity onPress={closeModal}>
            <Text>Закрыть</Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </TouchableOpacity>
    
    
  );
};

const styled = StyleSheet.create({
  oneCardWrapper: {
    height: "12%",
    flexDirection: "row",
    backgroundColor: "#1E90FF",
    justifyContent: "space-between",
    alignItems: "center",
  },
  wrapperForCardText: {
    marginLeft: 15,
  },
  wrapperForImage: {
    marginRight: 15,
  },
  bigText: {
    color: "white",
    fontSize: 24,
  },
  smallText: {
    color: "white",
    fontSize: 16,
  },
  modalContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
});

export default DivepoolCard;
