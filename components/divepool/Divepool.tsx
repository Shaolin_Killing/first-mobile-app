import React, { useEffect, useState } from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native"; 
import NavBar from "../NavBar/NavBar";
import { Asset } from "expo-asset";
const names = ['BB-01', 'BB-02', 'BB-A', 'BB-B', 'BB-C', 'BB-D', 'BB-E', 'BB-F', 'BB-G', 'BB-H', 'BB-J', 'BB-K', 'BB-L'];

function Divepool(): JSX.Element {
  const navigation = useNavigation();
  const [images, setImages] = useState<{ [key: string]: any }>({});
  
  useEffect(() => {
    const loadImages = async () => {
      const loadedImages: { [key: string]: any } = {};
      for (const name of names) {
        const img = await Asset.fromModule(require(`../../public/vfs2/BB/${name}.png`)).downloadAsync();
        loadedImages[name] = img;
      }
      setImages((prevImages) => ({
        ...prevImages,
        ...loadedImages
      }));
    };

    loadImages();
  }, []);

  const handleCategoryPress = (category: string) => {
    navigation.navigate("CategoryPage", { category: category, images: images });
  };

  return (
    <View>
      <View>
        <NavBar />
      </View>
      <View style={styled.text}>
        <Text>АЭРОТРУБА</Text>
      </View>
      <TouchableOpacity onPress={() => handleCategoryPress("vfs2")} style={styled.dodgerBlueContainer}>
        <Text>vfs2</Text>
        <Image
          style={styled.button}
          source={require("../images/play-button.png")}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => handleCategoryPress("vfs4")} style={styled.deepSkyBlueContainer}>
        <Text>vfs4</Text>
        <Image
          style={styled.button}
          source={require("../images/play-button.png")}
        />
      </TouchableOpacity >
      <TouchableOpacity onPress={() => handleCategoryPress("fs2")} style={styled.dodgerBlueContainer}>
        <Text>fs2</Text>
        <Image
          style={styled.button}
          source={require("../images/play-button.png")}
        />
      </TouchableOpacity>
      <TouchableOpacity onPress={() => handleCategoryPress("fs4")} style={styled.deepSkyBlueContainer}>
        <Text>fs4</Text>
        <Image
          style={styled.button}
          source={require("../images/play-button.png")}
        />
      </TouchableOpacity>
      
    </View>
  );
}

const styled = StyleSheet.create({
  text: {
    marginTop: 20,
    marginBottom: 5,
    marginLeft: 10,
  },
  titleOfImage: {

  },
  dodgerBlueContainer: {
    flexDirection: 'row',
    backgroundColor: "#1E90FF",
    height: '15%',
    width: '100%',
    marginTop: 15,
    marginBottom: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  deepSkyBlueContainer: {
    flexDirection: 'row',
    backgroundColor: "#00BFFF",
    height: '15%',
    width: '100%',
    marginTop: 15,
    marginBottom: 15,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  button: {
    height: 20,
    width: 20,
  },
});

export default Divepool;
