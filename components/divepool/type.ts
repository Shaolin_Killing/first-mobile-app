interface AssetType {
  hash: null | string;
  localUri: string;
  width: number;
  height: number;
  downloading: boolean;
}



