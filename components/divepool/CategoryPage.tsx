import React, {useState} from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Modal,
  ScrollView,
} from "react-native";
import NavBar from "../NavBar/NavBar";
import DivepoolCard from "./DivepoolCard";
const names = [
  "BB-01",
  "BB-02",
  "BB-A",
  "BB-B",
  "BB-C",
  "BB-D",
  "BB-E",
  "BB-F",
  "BB-G",
  "BB-H",
  "BB-J",
  "BB-K",
  "BB-L",
];


function CategoryPage({ route }: { route: any }): JSX.Element {
  const { category, images } = route.params;
  // const [selectedDivepool, setSelectedDivepool] = useState<string | null>(null);

  // const handleDivepoolPress = (divepoolName: string) => {
  //   setSelectedDivepool(divepoolName);
  // };

  return (
    <View>
      <View>
        <NavBar />
      </View>

      {/* Render the settings header */}
      <View style={styled.wrapperSet}>
        <TouchableOpacity>
          <Image
            style={styled.buttonBack}
            source={require("../images/back-button.png")}
          ></Image>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styled.textSet}>НАСТРОЙКИ</Text>
        </TouchableOpacity>
      </View>

      {/* Render divepool cards*/}
      
        {names.map((name) => (
          <DivepoolCard key={name} imageName={name} image={images[name]} name={name} />
        ))}
      

      

    </View>
  );
}
const { width, height } = Dimensions.get("window");

const styled = StyleSheet.create({
  
  textSet: {
    marginRight: 20,
  },
  buttonBack: {
    width: 20,
    height: 20,
    marginTop: 15,
    marginBottom: 15,
    marginLeft: 20,
  },
  wrapperSet: {
    flexDirection: "row",
    height: 30,
    width: "100%",
    marginTop: 15,
    marginBottom: 15,
    justifyContent: "space-between",
    alignItems: "center",
  },
});

export default CategoryPage;







{/* Render divepool cards*/}
      {/*
      {names.map((name) => (
          <TouchableOpacity key={name} onPress={() => handleDivepoolPress(name)}>
            <DivepoolCard imageName={name} image={images[name]} />
          </TouchableOpacity>
        ))}
        */}

      {/* Render modal */}
      {/*
      <Modal visible={selectedDivepool !== null}>
        <ScrollView>
          <DivepoolCard
            imageName={selectedDivepool || ""}
            image={images[selectedDivepool || ""]}
            style={{ width: Dimensions.get("window").width, height: Dimensions.get("window").height }}
          />
        </ScrollView>
        <TouchableOpacity onPress={() => setSelectedDivepool(null)}>
          <Text>Close</Text>
        </TouchableOpacity>
      </Modal>
      */}


// const getDataFromFolder = async () => {
//   const folderPath = FileSystem.documentDirectory + 'flight-data/' + category;
//   const folderInfo = await FileSystem.getInfoAsync(folderPath);
//   if (folderInfo.exists && folderInfo.isDirectory) {
//     const files = await FileSystem.readDirectoryAsync(folderPath);
//     console.log(files);
//     // Используйте полученные файлы или выполните другие необходимые действия
//   }
// };




