import React, { useState } from "react";
import { useNavigation } from '@react-navigation/native';
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,
  Dimensions,
  TouchableWithoutFeedback,
  Modal, 
} from "react-native";

function NavBar(): JSX.Element {
  const [menuVisible, setMenuVisible] = useState(false)
  const navigation = useNavigation();

  const toggleMenu = () => {
    setMenuVisible(!menuVisible);
  };

  const handleDivepoolPress = () => {
    setMenuVisible(false);
    navigation.navigate('Divepool');
  };
  const handleGeneratorPress = () => {
    setMenuVisible(false);
    navigation.navigate('Generator');
  };
  const handleGripCounterPress = () => {
    setMenuVisible(false);
    navigation.navigate('GripCounter');
  };
  const handleQuizPress = () => {
    setMenuVisible(false);
    navigation.navigate('Quiz');
  };
  const handleAboutPress = () => {
    setMenuVisible(false);
    navigation.navigate('About');
  };
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.menu} onPress={toggleMenu}>
        <Text>☰</Text>
      </TouchableOpacity>
      <View style={styles.flyLeagueView}>
        <Text style={styles.flyLeagueText}>Flyleague</Text>
      </View>

      <Modal
        visible={menuVisible}
        animationType="slide"
        onRequestClose={toggleMenu}
        transparent
      >
        <TouchableWithoutFeedback onPress={toggleMenu}>
          <View style={styles.menuContainer}>
            <View style={styles.menuContent}>
              <TouchableOpacity style={styles.menuItem} onPress={() => {}}>
                <Text style={styles.menuItemText}>ГЛАВНАЯ</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuItem} onPress={handleDivepoolPress}>
                <Text style={styles.menuItemText}>ДАЙВПУЛ</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuItem} onPress={handleGeneratorPress}>
                <Text style={styles.menuItemText}>ГЕНЕРАТОР</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuItem} onPress={handleGripCounterPress}>
                <Text style={styles.menuItemText}>СЧЕТЧИК ЗАХВАТОВ</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuItem} onPress={handleQuizPress}>
                <Text style={styles.menuItemText}>ВИКТОРИНА</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.menuItem} onPress={handleAboutPress}>
                <Text style={styles.menuItemText}>О НАС</Text>
              </TouchableOpacity>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    </View>
  );
}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    width: width,
    height: height * 0.1,
    backgroundColor: "#1E90FF",
  },
  menu: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  flyLeagueView: {
    flex: 2,
    // backgroundColor: '#FFEFD5',
    alignItems: "center",
    justifyContent: "center",
  },
  flyLeagueText: {
    color: "black",
  },
  menuContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "flex-end",
    alingItems: "flex-start",
  },
  menuContent: {
    backgroundColor: "white",
    padding: 20,
    alignItems: 'flex-start'
  },
  menuItem: {
    paddingVertical: 10,
  },
  menuItemText: {
    fontSize: 18,
    color: "#1E90FF",
  },
});

export default NavBar;
