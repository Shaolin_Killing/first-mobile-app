import React from 'react'
import { StyleSheet, Text, View } from 'react-native';
import NavBar from '../NavBar/NavBar';

function MainPage(): JSX.Element {
  return (
    <View>
      <View >
        <NavBar /> 
      </View>
      <View >
        <Text >FlyLeague</Text>
        <Text>Летаем как умеем! Вот</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  badge: {
    flex: 1,
    backgroundColor: '#1E90FF',
  },
  flyLeagueText: {
    height: 300,
    width: 100,
    backgroundColor: 'yellow',
    marginTop: 20,
  },
  flyLeagueView: {
    backgroundColor: 'green',
  }
});

export default MainPage;